import json
import requests

from WeatherApp.keys import OPEN_WEATHER_API_KEY


def get_wether_data(zipcode):
    params = {
        "q": f"{zipcode}, US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params)
    content = json.loads(response.content)
    return content
    